package plugins.stef.examples;

import icy.canvas.IcyCanvas;
import icy.gui.frame.progress.FailedAnnounceFrame;
import icy.math.FPSMeter;
import icy.painter.Overlay;
import icy.plugin.abstract_.PluginActionable;
import icy.plugin.interface_.PluginThreaded;
import icy.sequence.Sequence;
import icy.sequence.SequenceEvent;
import icy.sequence.SequenceListener;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import com.github.sarxos.webcam.Webcam;

/**
 * Simple example of usage for the webcam Capture library
 * 
 * @author Stephane
 */
public class WebcamCaptureExamplePlugin extends PluginActionable implements SequenceListener, PluginThreaded
{
    class FPSOverlay extends Overlay
    {
        final FPSMeter fpsMeter;

        public FPSOverlay()
        {
            super("FPS overlay");

            fpsMeter = new FPSMeter();
        }

        public void update()
        {
            fpsMeter.update();
            painterChanged();
        }

        @Override
        public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
        {
            final Graphics g2 = g.create();

            g2.setColor(Color.yellow);
            g2.drawString(fpsMeter.getFPS() + " FPS", 10, 10);

            g2.dispose();
        }
    }

    final Sequence sequence;
    final FPSOverlay fpsOverlay;
    final Webcam webcam;
    volatile boolean running;

    public WebcamCaptureExamplePlugin()
    {
        super();

        sequence = new Sequence("Webcam capture");
        sequence.addListener(this);

        fpsOverlay = new FPSOverlay();
        sequence.addOverlay(fpsOverlay);

        webcam = Webcam.getDefault();
    }

    @Override
    public void run()
    {
        if (webcam != null)
        {
            addSequence(sequence);

            if (webcam.open())
            {
                try
                {
                    running = true;
                    while (running)
                    {
                        sequence.setImage(0, 0, webcam.getImage());
                        fpsOverlay.update();
                    }
                }
                finally
                {
                    webcam.close();
                }
            }
        }
        else
            new FailedAnnounceFrame("No webcam found !");
    }

    @Override
    public void sequenceChanged(SequenceEvent sequenceEvent)
    {
        // nothing here
    }

    @Override
    public void sequenceClosed(Sequence sequence)
    {
        running = false;
        sequence.removeListener(this);
    }
}
